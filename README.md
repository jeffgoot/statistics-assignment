# Subject: [41000-88, XP](http://www.rob-mcculloch.org/xp_2015/)
## Assignments in Business Statistics
### Assignment #1
{[Main source file](R/assignment1.R)}

* [1.1. Problem: Target Marketing Decision](R/assignment1_1.1.R)
* [1.2. Problem: Profit Calculation](R/assignment1_1.2.R)
* [1.3. Problem: Is iRecMer1 Related to purchase?](R/assignment1_1.3.R)
* [4.1.b. Problem: Discrete RV 1](R/assignment1_4.1.b.R)
* [4.2.b. and 4.2.c. Problem: Discrete RV 2](R/assignment1_4.2.R)

## Lecture Functions
* [Expected Value](R/lecture_functions.R) {[unit tests](tests/testthat/test_lecture_expected_value.R)}
* [Plot Defective Widgets](R/lecture_3_defective_widgets.R) 
* [Random Walk Model](R/lecture_4_random_walk.R)  {[unit tests](tests/testthat/test_lecture_randomwalk.R)}

- - -
Note: [I am new to R](http://www.gutierrez.ph). I'd appreciate [comments and suggestions](https://bitbucket.org/jeffgoot/statistics-assignment/commits/all).
