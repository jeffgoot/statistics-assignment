# 
# Generates random walk data with the specified number of samples.
# @param num_samples the number of samples to generate.
# @param probability the probability of 1
# @param the values to associate to 0 and 1 -- e.g. c(-0.125, 0.125)
#
random_walk <- function(num_samples, probability, tick_values) {
  # Generate the bernoulli draws
  ticks = rbinom(num_samples, 1, probability)
  
  # Generate the values for plotting.
  current_sum = 0
  values = unlist(lapply(ticks, FUN = function(it){ 
    current_sum <<- current_sum + tick_values[it+1]
  }))
  
  return(values)
}

# 
# 4. The Random Walk Model
# TODO: Implement using the equation...
#
plot_random_walk <- function(num_samples = 100, probability = 0.68, tick_values =  c(-0.125, 0.125)) {
  
  values = random_walk(num_samples, probability, tick_values)
  
  # Plot the points
  plot(values, 
       type = "b",
       xlab = "time index",
       ylab = "P")  
}

