## @param reponse_data The CSV data loaded using read.csv("http://www.rob-mcculloch.org/data/response-phat.csv
## @phat_max the maximum probability that the consumer will respond.
##
profit_calculation <- function(response_data, phat_max) {
  dotarget = as.factor(response_data$phat > phat_max)
  levels(dotarget) = c("NO","YES")
  retval <- table(dotarget, response_data$response)
  return(retval)
}



## 1.2. Profit Calculation
##
## Suppose we had targeted all customers with the response probability greater than .04.
## How much money would you have made?
## Hint: just change .02 to .04 in the previous R script.
## 
assignment_1.2 <- function() {
  response_data = read.csv("http://www.rob-mcculloch.org/data/response-phat.csv")
  probability = 0.04;
  new_data = profit_calculation(response_data, probability)
  
  money = new_data[2,1] * 40 - sum((new_data[2,]*0.8))
  cat("money would you have been made with cut-off of ", probability, "is $", money )
}  